package simulator

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/moby/moby/pkg/stdcopy"
	"github.com/slayercat/GoSNMPServer"
	"github.com/slayercat/gosnmp"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/sirupsen/logrus"
)

type nodeVBMC interface {
	getNodeCount() int
	stopVBMC(outlet int) (err error)
	startVBMC(outlet int) (err error)
	getVBMCStatus(outlet int) (vbmc_status string, err error)
	getLogger() GoSNMPServer.ILogger
}

type baseVBMC struct {
	logger GoSNMPServer.ILogger
}

type dockerNodeVBMC struct {
	nodeVBMC
	baseVBMC
	cli             client.Client
	ctx             context.Context
	containerPrefix string
	nodePrefix      string
}

func (info *dockerNodeVBMC) runInContainer(outlet int, cmd []string) (output []byte, exitCode int, err error) {
	output = []byte{}
	exitCode = 0

	execConfig := types.ExecConfig{
		AttachStdout: true,
		AttachStderr: true,
		Cmd:          cmd,
	}

	containerName := fmt.Sprintf("%s%d", info.containerPrefix, outlet-1)

	info.logger.Infof("[%s] running command %#v", containerName, cmd)

	cresp, err := info.cli.ContainerExecCreate(info.ctx, containerName, execConfig)

	if err != nil {
		return
	}

	resp, err := info.cli.ContainerExecAttach(info.ctx, cresp.ID, types.ExecStartCheck{})

	if err != nil {
		return
	}

	defer resp.Close()

	// read the output
	var outBuf, errBuf bytes.Buffer
	outputDone := make(chan error)

	go func() {
		// StdCopy demultiplexes the stream into two buffers
		_, err = stdcopy.StdCopy(&outBuf, &errBuf, resp.Reader)
		outputDone <- err
	}()

	select {
	case err = <-outputDone:
		if err != nil {
			return
		}
		break

	case <-info.ctx.Done():
		err = info.ctx.Err()
		return
	}

	output, err = io.ReadAll(&outBuf)
	if err != nil {
		return
	}

	iresp, err := info.cli.ContainerExecInspect(info.ctx, cresp.ID)
	if err != nil {
		return
	}

	exitCode = iresp.ExitCode

	if exitCode != 0 {
		err_output, _ := io.ReadAll(&errBuf)
		err = fmt.Errorf("%s", string(err_output))
	}

	return
}

func (info *dockerNodeVBMC) runVBMCCommand(outlet int, cmd []string) (output []byte, ret int, err error) {
	return info.runInContainer(outlet, append([]string{"vbmc"}, cmd...))
}

func (info dockerNodeVBMC) getVBMCStatus(outlet int) (vbmc_status string, err error) {

	output, ret, err := info.runVBMCCommand(outlet, []string{"list", "-f=json"})

	if ret != 0 || err != nil {
		return
	}

	var dat [](map[string]interface{})
	err = json.Unmarshal(output, &dat)

	if err == nil {
		if len(dat) > 0 {
			first_status := dat[0]
			if val, ok := first_status["Status"]; ok {
				vbmc_status = fmt.Sprint(val)
			} else {
				err = fmt.Errorf("status not in object %#v", first_status)
			}
		} else {
			err = errors.New("no objects")
		}
	}

	info.logger.Infof("Outlet %d status %s", outlet, vbmc_status)

	return
}

func (info dockerNodeVBMC) stopVBMC(outlet int) (err error) {
	info.logger.Infof("Stopping outlet %d", outlet)
	_, _, err = info.runVBMCCommand(outlet, []string{"stop", fmt.Sprintf("%s%d", info.nodePrefix, outlet-1)})
	return
}

func (info dockerNodeVBMC) startVBMC(outlet int) (err error) {
	info.logger.Infof("Starting outlet %d", outlet)
	_, _, err = info.runVBMCCommand(outlet, []string{"start", fmt.Sprintf("%s%d", info.nodePrefix, outlet-1)})
	return
}

func (info dockerNodeVBMC) getNodeCount() (count int) {

	containers, err := info.cli.ContainerList(info.ctx, types.ContainerListOptions{})
	if err != nil {
		panic(err)
	}

	count = 0

	for _, container := range containers {
		if strings.Contains(container.Names[0], info.containerPrefix) {
			count += 1
		}
	}

	return
}

func (info dockerNodeVBMC) getLogger() GoSNMPServer.ILogger {
	return info.logger
}

func newDockerNodeVBMC(ctx context.Context, logger GoSNMPServer.ILogger) dockerNodeVBMC {

	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}

	return dockerNodeVBMC{
		ctx:             ctx,
		cli:             *cli,
		containerPrefix: "vbmc-emulator-node-",
		nodePrefix:      "node-",
		baseVBMC: baseVBMC{
			logger: logger,
		},
	}
}

type PowerState int

const (
	Undefined  PowerState = 0
	PoweredOn  PowerState = 1
	PoweredOff PowerState = 2
)

type SimulatorVBMC struct {
	nodeVBMC
	baseVBMC
	NodeStatus []PowerState
}

func (info SimulatorVBMC) getNodeCount() int {
	return len(info.NodeStatus)
}

func (info *SimulatorVBMC) stopVBMC(outlet int) (err error) {
	info.logger.Infof("Stopping outlet %d", outlet)
	if outlet-1 < len(info.NodeStatus) {
		info.NodeStatus[outlet-1] = PoweredOff
	} else {
		err = fmt.Errorf("no such node %d", outlet)
	}
	return
}

func (info *SimulatorVBMC) startVBMC(outlet int) (err error) {
	info.logger.Infof("Starting outlet %d", outlet)
	if outlet-1 < len(info.NodeStatus) && outlet > 0 {
		info.NodeStatus[outlet-1] = PoweredOn
		time.Sleep(500 * time.Millisecond)
	} else {
		err = fmt.Errorf("no such node %d", outlet)
	}
	return
}

func (info *SimulatorVBMC) getVBMCStatus(outlet int) (vbmc_status string, err error) {
	if outlet-1 < len(info.NodeStatus) && outlet > 0 {
		switch info.NodeStatus[outlet-1] {
		case PoweredOn:
			vbmc_status = "running"
		case PoweredOff:
			vbmc_status = "down"
		default:
			vbmc_status = "undefined"
		}
		time.Sleep(300 * time.Millisecond)
	} else {
		err = fmt.Errorf("no such outlet %d", outlet)
	}

	info.logger.Infof("Outlet %d status %s (%#v)", outlet, vbmc_status, err)
	return
}

func (info SimulatorVBMC) getLogger() GoSNMPServer.ILogger {
	return info.logger
}

func NewSimulatorVBMC(outletCount int, logger GoSNMPServer.ILogger) *SimulatorVBMC {
	return &SimulatorVBMC{
		NodeStatus: make([]PowerState, outletCount),
		baseVBMC:   baseVBMC{logger: logger},
	}
}

func oidOutletStatus(nodes nodeVBMC, outlet int, set bool, input_value interface{}) (value interface{}, err error) {
	status, err := nodes.getVBMCStatus(outlet)

	if err == nil {

		switch status {
		case "running":
			value = GoSNMPServer.Asn1IntegerWrap(1)
		case "down":
			value = GoSNMPServer.Asn1IntegerWrap(2)
		case "undefined":
			value = GoSNMPServer.Asn1IntegerWrap(0)
		default:
			err = fmt.Errorf("unknown status %v", status)
		}
	}

	if err == nil && set && input_value != nil {

		if input_value != value {
			input_value_int := GoSNMPServer.Asn1IntegerUnwrap(input_value)

			switch input_value_int {
			case 1:
				err = nodes.startVBMC(outlet)
			case 2:
				err = nodes.stopVBMC(outlet)
			default:
				err = fmt.Errorf("invalid state: %v", input_value)
			}
		}
	}

	return
}

func createPDUValueControlItems(nodes nodeVBMC) []*GoSNMPServer.PDUValueControlItem {
	count := nodes.getNodeCount()

	items := make([]*GoSNMPServer.PDUValueControlItem, count)

	for i := 1; i <= count; i++ {
		oid_num := i
		items[i-1] = &GoSNMPServer.PDUValueControlItem{
			OID:   fmt.Sprintf("1.3.6.1.4.1.318.1.1.4.4.2.1.3.%d", i),
			Type:  gosnmp.Integer,
			OnGet: func() (value interface{}, err error) { return oidOutletStatus(nodes, oid_num, false, nil) },
			OnSet: func(value interface{}) error { _, err := oidOutletStatus(nodes, oid_num, true, value); return err },
		}
	}

	return items
}

func NewLogger() GoSNMPServer.ILogger {
	var logger = logrus.New()
	logger.Out = os.Stdout
	logger.Level = logrus.InfoLevel
	return GoSNMPServer.WrapLogrus(logger)
}

func NewMasterAgent(nodes nodeVBMC, userName string, authenticationPassphrase string, privacyPassphrase string) GoSNMPServer.MasterAgent {

	return GoSNMPServer.MasterAgent{
		Logger: nodes.getLogger(),
		SecurityConfig: GoSNMPServer.SecurityConfig{
			AuthoritativeEngineBoots: 1,
			Users: []gosnmp.UsmSecurityParameters{
				{
					UserName:                 "control",
					AuthenticationProtocol:   gosnmp.MD5,
					PrivacyProtocol:          gosnmp.DES,
					AuthenticationPassphrase: "123456781234567",
					PrivacyPassphrase:        "123456781234567",
				},
			},
		},
		SubAgents: []*GoSNMPServer.SubAgent{
			{
				OIDs: createPDUValueControlItems(nodes),
			},
		},
	}
}

func RunServer(master GoSNMPServer.MasterAgent, ip string, port uint16) {
	server := GoSNMPServer.NewSNMPServer(master)
	err := server.ListenUDP("udp", fmt.Sprintf("%s:%d", ip, port))
	if err != nil {
		panic(client.ErrRedirect)
	}
	err = server.ServeForever()
	if err != nil {
		panic(err)
	}
}

func Run(listenAddress string) {
	GoSNMPServer.NewDefaultLogger()
	logger := NewLogger()
	nodes := newDockerNodeVBMC(context.Background(), logger)
	master := NewMasterAgent(nodes, "control", "123456781234567", "123456781234567")
	RunServer(master, listenAddress, 1161)
}
