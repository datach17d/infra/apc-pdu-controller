package main

import (
	"flag"

	"gitlab.com/datach17d/infra/apc-pdu-controller/simulator/pkg/simulator"
)

func main() {

	listenAddress := flag.String("listen", "127.0.0.1", "Listen address")
	flag.Parse()

	simulator.Run(*listenAddress)
}
