
.PHONY: all

all: build test vet lint

test:
	$(MAKE) -C controller $@

build:
	$(MAKE) -C controller $@
	$(MAKE) -C simulator $@

clean:
	$(MAKE) -C controller $@
	$(MAKE) -C simulator $@
	$(RM) ./gl-code-quality-report.json

vet:
	$(MAKE) -C controller $@
	$(MAKE) -C simulator $@

lint:
	$(MAKE) -C controller $@
	$(MAKE) -C simulator $@
	@ jq -s 'add' controller/gl-code-quality-report.json simulator/gl-code-quality-report.json | tee ./gl-code-quality-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
