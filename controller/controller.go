package main

import (
	"context"
	"log"
	"os"

	metal3v1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	"gitlab.com/datach17d/infra/apc-pdu-controller/controller/pkg/controller"
	clientsetscheme "k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	k8sclient "sigs.k8s.io/controller-runtime/pkg/client"
)

var version = "dev"

func createKubernetesClient() (client k8sclient.WithWatch, err error) {
	default_path := clientcmd.NewDefaultClientConfigLoadingRules().GetDefaultFilename()

	log.Printf("Trying kubeconfig: %s", default_path)
	kubeConfig, err := clientcmd.BuildConfigFromFlags("", default_path)

	if err != nil {
		log.Printf("Error: %v\n", err)

		kubeConfig, err = rest.InClusterConfig()

		if err != nil {
			return
		}

		log.Printf("Using in cluster config...")
	}

	log.Print("Adding metal3 scheme...")

	err = metal3v1alpha1.AddToScheme(clientsetscheme.Scheme)
	if err != nil {
		return
	}

	log.Print("Creating client...")

	client, err = k8sclient.NewWithWatch(kubeConfig, k8sclient.Options{Scheme: clientsetscheme.Scheme})

	return
}

func createConfig(path string) (config controller.APCConfig, err error) {
	var configFile *os.File
	if configFile, err = os.Open(path); err != nil {
		return
	}
	defer configFile.Close()

	config, err = controller.NewAPCConfig(configFile)

	return
}

func main() {
	log.Printf("Version: %s", version)

	client, err := createKubernetesClient()

	if err != nil {
		log.Panicf("Error: %v", err)
	}

	config, err := createConfig("config.yaml")

	if err != nil {

		if os.IsNotExist(err) {
			log.Printf("No config.yaml found in local directory, trying /etc/config...")

			config, err = createConfig("/etc/config/config.yaml")

			if err != nil {
				log.Panicf("Error: %v", err)
			}
		} else {
			log.Panicf("Error: %v", err)
		}
	}

	err = controller.Run(client, config, context.Background())

	if err != nil {
		log.Panicf("Error: %v", err)
	}
}
