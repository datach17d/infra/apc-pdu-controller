package controller

import (
	"context"
	"fmt"
	"io"
	"log"
	"sync"
	"time"

	metal3v1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	"gopkg.in/yaml.v3"

	k8sclient "sigs.k8s.io/controller-runtime/pkg/client"

	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/watch"

	g "github.com/gosnmp/gosnmp"
)

type PowerState int

const (
	Undefined  PowerState = 0
	PoweredOn  PowerState = 1
	PoweredOff PowerState = 2
)

func (ps PowerState) String() string {
	switch ps {
	case Undefined:
		return "Undefined"
	case PoweredOn:
		return "Powered On"
	case PoweredOff:
		return "Powered Off"
	}

	return fmt.Sprintf("Unknown (%d)", ps)
}

type APCConfig struct {
	Nodes   []bmhNode    `yaml:"nodes"`
	Clients []snmpClient `yaml:"servers"`
}

func NewAPCConfig(stream io.Reader) (config APCConfig, err error) {
	d := yaml.NewDecoder(stream)

	if err = d.Decode(&config); err != nil {
		return
	}

	err = config.createClients()

	return
}

func (config *APCConfig) getSNMPClientsByName(name string) (*snmpClient, error) {
	for i, c := range config.Clients {
		if c.Name == name {

			return &config.Clients[i], nil
		}
	}

	return nil, fmt.Errorf("no sever \"%s\" configured", name)
}

func (config *APCConfig) createClients() (err error) {
	var client *snmpClient
	for i, node := range config.Nodes {
		client, err = config.getSNMPClientsByName(node.ServerName)
		if err != nil {
			return
		}

		config.Nodes[i].pduSNMP = client.createClient()
		config.Nodes[i].powerAction = make(chan PowerState)
	}

	return
}

func (config *APCConfig) connectClients() (err error) {
	for i := range config.Clients {
		if err = config.Clients[i].Client.Connect(); err != nil {
			return
		}
	}
	return
}

type snmpClient struct {
	Name                      string `yaml:"name"`
	IP                        string `yaml:"ip"`
	Port                      uint16 `yaml:"port"`
	UserName                  string `yaml:"user"`
	AuthtenticationPassPhrase string `yaml:"auth"`
	PrivacyPassphrase         string `yaml:"privacy"`
	Client                    *g.GoSNMP
	mu                        sync.Mutex
}

func (config *snmpClient) createClient() *snmpClient {
	if config.Client == nil {
		config.Client = NewSNMPClient(config.IP, config.Port, config.UserName, config.AuthtenticationPassPhrase, config.PrivacyPassphrase)
	}
	return config
}

func (s *snmpClient) getPowerState(OutletOID string) (result PowerState, err error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	val, err := s.Client.Get([]string{OutletOID})
	if err == nil {
		result = PowerState(g.ToBigInt(val.Variables[0].Value).Int64())
	}
	return
}

func (s *snmpClient) setPowerState(OutletOID string, state PowerState) (result PowerState, err error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	result = Undefined
	r, err := s.Client.Set([]g.SnmpPDU{{
		Name:  OutletOID,
		Type:  g.Integer,
		Value: int(state),
	}})

	if err == nil {
		result = PowerState(g.ToBigInt(r.Variables[0].Value).Int64())
	}

	return
}

func NewSNMPClient(targetIP string, targetPort uint16, userName string, authenticationPassphrase string, privacyPassphrase string) *g.GoSNMP {
	return &g.GoSNMP{
		Target:        targetIP,
		Port:          targetPort,
		Version:       g.Version3,
		SecurityModel: g.UserSecurityModel,
		MsgFlags:      g.AuthPriv,
		Timeout:       time.Duration(2) * time.Second,
		SecurityParameters: &g.UsmSecurityParameters{UserName: userName,
			AuthenticationProtocol:   g.MD5,
			AuthenticationPassphrase: authenticationPassphrase,
			PrivacyProtocol:          g.DES,
			PrivacyPassphrase:        privacyPassphrase,
		},
		Retries: 3,
	}
}

type bmhNode struct {
	NodeNamespace string `yaml:"namespace"`
	NodeName      string `yaml:"name"`
	ServerName    string `yaml:"server"`
	OutletOID     string `yaml:"oid"`
	HoldOffTime   int64  `yaml:"holdoff"`

	pduSNMP                *snmpClient
	powerAction            chan PowerState
	currentState           PowerState
	currentStateLastUpdate int64
}

func (node bmhNode) log(format string, v ...any) {
	log.Printf("[%s/%s] %s", node.NodeNamespace, node.NodeName, fmt.Sprintf(format, v...))
}

func (node *bmhNode) watchNode(client k8sclient.WithWatch, ctx context.Context) {

	go node.powerConsumer(ctx)

	for {
		bmcList := &metal3v1alpha1.BareMetalHostList{}
		bmcWatchOptions := &k8sclient.ListOptions{
			FieldSelector: fields.OneTermEqualSelector("metadata.name", node.NodeName),
			Namespace:     node.NodeNamespace,
		}

		node.log("Starting watch on %#v...", bmcWatchOptions)

		bmcWatch, err := client.Watch(ctx, bmcList, bmcWatchOptions)

		if err != nil {
			node.log("watch error: %#v", err)
			node.log("restarting watch...")
			time.Sleep(5 * time.Second)
		} else {
		watch_loop:
			for {

				select {
				case event, ok := <-bmcWatch.ResultChan():
					if ok {
						node.log("Got event: %#v", event)

						switch event.Type {
						case watch.Added:
							fallthrough
						case watch.Modified:
							bmh := event.Object.(*metal3v1alpha1.BareMetalHost)
							node.log("BMH status: \n  name: %s\n  spec.online: %v\n  status.PoweredOn: %v\n  status.Provisioning.State: %#v", bmh.Name, bmh.Spec.Online, bmh.Status.PoweredOn, bmh.Status.Provisioning.State)
							if bmh.Status.Provisioning.State != "deleteing" {
								action := PoweredOn
								if !bmh.Spec.Online && !bmh.Status.PoweredOn && bmh.Status.Provisioning.State == "available" {
									action = PoweredOff
								}
								node.powerAction <- action
							}
						case watch.Deleted:
							node.log("Node deleted, powering off")
							node.powerAction <- PoweredOff
						case watch.Error:
							node.log("Watch error: %#v", event.Object)
						}

					} else {
						node.log("Event returned not ok - restarting...")
						time.Sleep(1 * time.Second)
						break watch_loop
					}
				case <-ctx.Done():
					node.log("Shutting down")
					return
				}
			}
		}
	}
}

func (node *bmhNode) powerConsumer(ctx context.Context) {
	for {
		node.log("Waiting for power actions")
		select {
		case action := <-node.powerAction:
			timedOut := action != PoweredOff
			nextAction := action
			node.log("Got action %s", action)
			for !timedOut {
				if action == PoweredOff {
					select {
					case nextAction = <-node.powerAction:
						node.log("Got action %v in hold off", nextAction)
					case <-time.After(time.Duration(node.HoldOffTime) * time.Second):
						node.log("Hold off reached - no more actions")
						timedOut = true
					case <-ctx.Done():
						node.log("Shutting down")
						return
					}
				}

				if timedOut {
					action = nextAction
				}
			}
			node.log("Setting state to %s", action)
			node.setPowerState(action)
		case <-ctx.Done():
			node.log("Shutting down")
			return
		}
	}
}

func (node *bmhNode) setPowerState(state PowerState) (err error) {
	if time.Now().Unix() > node.currentStateLastUpdate+node.HoldOffTime {
		node.log("Updating power state from %s", node.OutletOID)
		node.currentState, err = node.pduSNMP.getPowerState(node.OutletOID)
		node.currentStateLastUpdate = time.Now().Unix()
		if err != nil {
			node.log("Error getting OID %s: %v", node.OutletOID, err)
		}
	}

	if state != node.currentState {
		node.log("Transition state %s -> %s", node.currentState, state)
		node.currentState = state
	}

	node.currentState, err = node.pduSNMP.setPowerState(node.OutletOID, node.currentState)
	if err != nil {
		node.log("Error setting OID %s: %v", node.OutletOID, err)
	}
	return
}

func Run(client k8sclient.WithWatch, config APCConfig, ctx context.Context) (err error) {

	if err = config.connectClients(); err != nil {
		return
	}

	for i := range config.Nodes {
		go config.Nodes[i].watchNode(client, ctx)
	}

	<-ctx.Done()
	return
}
