package controller

import (
	"context"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/gosnmp/gosnmp"
	metal3v1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"

	clientsetscheme "k8s.io/client-go/kubernetes/scheme"

	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"testing"

	k8sclient "sigs.k8s.io/controller-runtime/pkg/client"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"github.com/stretchr/testify/assert"

	"gitlab.com/datach17d/infra/apc-pdu-controller/simulator/pkg/simulator"

	"github.com/slayercat/GoSNMPServer"
)

func waitSNMPServerReady(t *testing.T, snmpClient *gosnmp.GoSNMP, master GoSNMPServer.MasterAgent) {
	err := snmpClient.Connect()
	if err != nil {
		t.Fatalf("Connect() err: %v", err)
	}

	for i := 0; ; i++ {
		_, err = snmpClient.Get([]string{master.SubAgents[0].OIDs[0].OID})

		if err == nil {
			break
		}

		if i > 10 {
			t.Fatalf("Server not started...")
		}

		time.Sleep(50 * time.Millisecond)
	}
}

func createSimulator(t *testing.T, port uint16) (snmpClient *gosnmp.GoSNMP, nodes *simulator.SimulatorVBMC) {
	logger := simulator.NewLogger()
	nodes = simulator.NewSimulatorVBMC(3, logger)
	master := simulator.NewMasterAgent(nodes, "control", "123456781234567", "123456781234567")
	snmpClient = NewSNMPClient("127.0.0.1", port, "control", "123456781234567", "123456781234567")

	go simulator.RunServer(master, "127.0.0.1", port)
	waitSNMPServerReady(t, snmpClient, master)

	return
}

func assertPowerState(assert *assert.Assertions, nodes *simulator.SimulatorVBMC, expectedPowerState simulator.PowerState, holdOff time.Duration) {
	time.Sleep(holdOff)

	i := 0

	for ; ; i++ {
		if nodes.NodeStatus[0] == expectedPowerState || i > 10 {
			break
		}
		time.Sleep(time.Duration(500 * time.Millisecond))
	}

	assert.Equalf(expectedPowerState, nodes.NodeStatus[0], "power state unexpected (retries: %d)", i)
}

func runBackground(client k8sclient.WithWatch, config APCConfig, ctx context.Context) {
	err := Run(client, config, ctx)
	if err != nil {
		panic(err)
	}
}

func TestConfig(t *testing.T) {
	assert := assert.New(t)

	yamlStream := strings.NewReader(`
nodes:
  - name: node0
    namespace: virt
    server: test1
    oid: "1.3.6.1.4.1.318.1.1.4.4.2.1.3.1"
    holdoff: 5
servers:
  - name: test1
    ip: 127.0.0.1
    port: 40003
    user: control
    auth: "123456781234567"
    privacy: "123456781234567"`)

	config, err := NewAPCConfig(yamlStream)
	assert.NoError(err, "unexpected error")
	assert.NotEmpty(config.Clients, "missing client")
	assert.NotEmpty(config.Nodes, "missing nodes")
	assert.NotNil(config.Clients[0].Client, "client wasn't created")
	assert.NotNil(config.Nodes[0].pduSNMP, "client wasn't set for node")
	assert.Equal(config.Clients[0].Client, config.Nodes[0].pduSNMP.Client)
}

func TestSimpleSNMP(t *testing.T) {
	assert := assert.New(t)

	holdOffTime := int64(5)

	sim, simulatorNodes := createSimulator(t, 40001)
	defer sim.Conn.Close()

	node := bmhNode{
		NodeNamespace: "virt",
		NodeName:      "node0",
		powerAction:   make(chan PowerState),
		pduSNMP: &snmpClient{
			Client: sim,
			mu:     sync.Mutex{},
		},
		OutletOID:   "1.3.6.1.4.1.318.1.1.4.4.2.1.3.1",
		HoldOffTime: holdOffTime,
	}
	go node.powerConsumer(context.Background())

	assertPowerState(assert, simulatorNodes, simulator.Undefined, time.Duration(0))

	node.powerAction <- PoweredOn
	assertPowerState(assert, simulatorNodes, simulator.PoweredOn, time.Duration(0))

	node.powerAction <- PoweredOff
	assertPowerState(assert, simulatorNodes, simulator.PoweredOff, time.Duration(holdOffTime+1)*time.Second)
}

func TestBMHwithSNMP(t *testing.T) {
	assert := assert.New(t)

	holdOffTime := int64(1)

	bmhResource := &metal3v1alpha1.BareMetalHost{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "node0",
			Namespace: "virt",
		},
		Status: metal3v1alpha1.BareMetalHostStatus{
			Provisioning: metal3v1alpha1.ProvisionStatus{
				State: metal3v1alpha1.StateAvailable,
			},
			PoweredOn: false,
		},
		Spec: metal3v1alpha1.BareMetalHostSpec{
			Online: false,
		},
	}
	_ = metal3v1alpha1.AddToScheme(clientsetscheme.Scheme)

	client := fake.NewClientBuilder().WithScheme(clientsetscheme.Scheme).Build()

	sim, simulatorNodes := createSimulator(t, 40002)
	defer sim.Conn.Close()

	node := bmhNode{
		NodeNamespace: "virt",
		NodeName:      "node0",
		powerAction:   make(chan PowerState),
		pduSNMP: &snmpClient{
			Client: sim,
			mu:     sync.Mutex{},
		},
		OutletOID:   "1.3.6.1.4.1.318.1.1.4.4.2.1.3.1",
		HoldOffTime: holdOffTime,
	}

	go node.watchNode(client, context.Background())

	time.Sleep(100 * time.Millisecond)

	err := client.Create(context.Background(), bmhResource, &k8sclient.CreateOptions{})
	assert.NoError(err, "unexpected error")

	assertPowerState(assert, simulatorNodes, simulator.PoweredOff, time.Duration(holdOffTime)*time.Second)

	err = client.Get(context.Background(), k8sclient.ObjectKeyFromObject(bmhResource), bmhResource, &k8sclient.GetOptions{})
	assert.NoError(err, "unexpected error")

	bmhResource.Status.PoweredOn = true

	err = client.Update(context.Background(), bmhResource, &k8sclient.UpdateOptions{})
	assert.NoError(err, "unexpected error")

	assertPowerState(assert, simulatorNodes, simulator.PoweredOn, time.Duration(1)*time.Second)
}

func TestE2EColdStart(t *testing.T) {
	assert := assert.New(t)
	holdOffTime := int64(1)
	port := uint16(40003)
	ctx, cancel := context.WithCancel(context.Background())

	yamlStream := strings.NewReader(fmt.Sprintf(`
nodes:
  - name: node0
    namespace: virt
    server: test1
    oid: "1.3.6.1.4.1.318.1.1.4.4.2.1.3.1"
    holdoff: %d
servers:
  - name: test1
    ip: 127.0.0.1
    port: %d
    user: control
    auth: "123456781234567"
    privacy: "123456781234567"`, holdOffTime, port))

	config, err := NewAPCConfig(yamlStream)
	assert.NoError(err, "unexpected error")

	snmpClient, simulatorNodes := createSimulator(t, port)
	defer snmpClient.Conn.Close()

	bmhResource := &metal3v1alpha1.BareMetalHost{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "node0",
			Namespace: "virt",
		},
		Status: metal3v1alpha1.BareMetalHostStatus{
			Provisioning: metal3v1alpha1.ProvisionStatus{
				State: metal3v1alpha1.StateAvailable,
			},
			PoweredOn: true,
		},
		Spec: metal3v1alpha1.BareMetalHostSpec{
			Online: false,
		},
	}
	_ = metal3v1alpha1.AddToScheme(clientsetscheme.Scheme)
	client := fake.NewClientBuilder().WithScheme(clientsetscheme.Scheme).Build()

	go runBackground(client, config, ctx)
	time.Sleep(100 * time.Millisecond)

	// Initial power state for newly created
	err = client.Create(ctx, bmhResource, &k8sclient.CreateOptions{})
	assert.NoError(err, "unexpected error")
	assertPowerState(assert, simulatorNodes, simulator.PoweredOn, time.Duration(0))

	// Power off in available
	err = client.Get(ctx, k8sclient.ObjectKeyFromObject(bmhResource), bmhResource, &k8sclient.GetOptions{})
	assert.NoError(err, "unexpected error")
	bmhResource.Status.PoweredOn = false
	err = client.Update(ctx, bmhResource, &k8sclient.UpdateOptions{})
	assert.NoError(err, "unexpected error")
	assertPowerState(assert, simulatorNodes, simulator.PoweredOff, time.Duration(holdOffTime)*time.Second)

	cancel()

	time.Sleep(100 * time.Millisecond)
}

func TestE2EDeleted(t *testing.T) {
	assert := assert.New(t)
	holdOffTime := int64(1)
	port := uint16(40004)
	ctx, cancel := context.WithCancel(context.Background())

	yamlStream := strings.NewReader(fmt.Sprintf(`
nodes:
  - name: node0
    namespace: virt
    server: test1
    oid: "1.3.6.1.4.1.318.1.1.4.4.2.1.3.1"
    holdoff: %d
servers:
  - name: test1
    ip: 127.0.0.1
    port: %d
    user: control
    auth: "123456781234567"
    privacy: "123456781234567"`, holdOffTime, port))

	config, err := NewAPCConfig(yamlStream)
	assert.NoError(err, "unexpected error")

	snmpClient, simulatorNodes := createSimulator(t, port)
	defer snmpClient.Conn.Close()

	bmhResource := &metal3v1alpha1.BareMetalHost{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "node0",
			Namespace: "virt",
		},
		Status: metal3v1alpha1.BareMetalHostStatus{
			Provisioning: metal3v1alpha1.ProvisionStatus{
				State: metal3v1alpha1.StateAvailable,
			},
			PoweredOn: true,
		},
		Spec: metal3v1alpha1.BareMetalHostSpec{
			Online: false,
		},
	}
	_ = metal3v1alpha1.AddToScheme(clientsetscheme.Scheme)
	client := fake.NewClientBuilder().WithScheme(clientsetscheme.Scheme).Build()

	go runBackground(client, config, ctx)
	time.Sleep(100 * time.Millisecond)

	// Initial power state for newly created
	err = client.Create(ctx, bmhResource, &k8sclient.CreateOptions{})
	assert.NoError(err, "unexpected error")
	assertPowerState(assert, simulatorNodes, simulator.PoweredOn, time.Duration(0))

	// Deleting state for bmh
	err = client.Get(ctx, k8sclient.ObjectKeyFromObject(bmhResource), bmhResource, &k8sclient.GetOptions{})
	assert.NoError(err, "unexpected error")
	bmhResource.Status.PoweredOn = false
	bmhResource.Status.Provisioning.State = metal3v1alpha1.StateDeleting
	err = client.Update(ctx, bmhResource, &k8sclient.UpdateOptions{})
	assert.NoError(err, "unexpected error")
	assertPowerState(assert, simulatorNodes, simulator.PoweredOn, time.Duration(holdOffTime)*time.Second)

	// Delete object
	err = client.Delete(ctx, bmhResource, &k8sclient.DeleteAllOfOptions{})
	assert.NoError(err, "unexpected error")
	assertPowerState(assert, simulatorNodes, simulator.PoweredOff, time.Duration(holdOffTime*5)*time.Second)

	cancel()

	time.Sleep(100 * time.Millisecond)
}
